# -*- coding: utf-8 -*-

"""Client class definition for cpasswords protocol.

(WIP)
"""

from cpasswords import client as _old_client

class Client(object):
    """A client connection."""

    verbose = False

    def __init__(self, serverdata):
        """
        serverdata should be a classic dict object (from eg a clientconfig
        module)
        """
        self.serverdata = serverdata

    def put_file(self, data):
        """Send file to server"""
        # TODO put code here
        _old_client.put_files(self, [data])
