#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Configuration Serveur de cpasswords.

Version de Tudor
"""

#: Pour override le nom si vous voulez renommer la commande
cmd_name = 'cpasswords'

#: Chemin vers la commande sendmail
sendmail_cmd = '/usr/lib/sendmail'

#: Répertoire de stockage des mots de passe
STORE = '/home/dstan/passwords/data'

#: Ce serveur est-il read-only (on ne peut pas y modifier les mots de passe)
READONLY = False

#: Expéditeur du mail de notification
CRANSP_MAIL = u"%s <dstan+cpasswords@crans.org>" % (cmd_name,)

#: Destinataire du mail de notification
DEST_MAIL = u"dstan+cpasswords@crans.org"

#: Mapping des utilisateurs et de leurs (mail, fingerprint GPG)
KEYS = {
    u'dstan': (u'daniel.stan@crans.org', u'90520CFDE846E7651A1B751FBC9BF8456E1C820B'),
}


_ME = [u'dstan']

#: Les roles utilisés pour savoir qui a le droit le lire/écrire quoi
ROLES = {
    'moi': _ME,
    'moi-w': _ME,
}

BACKUP_SERVERS = {
    'gladys': {
        'server_cmd': ['/usr/bin/ssh', 'home.b2moo.fr', '/home/dstan/cranspasswords/serverconfigs/tudor/cpasswords-server', ],
        'keep-alive': True,
    },
}


BACKUP_ROLES = {
    'moi': ['gladys'],
}
