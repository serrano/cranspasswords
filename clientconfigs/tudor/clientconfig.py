#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Configuration du client cranspasswords """

import os


#: Liste des serveurs sur lesquels ont peut récupérer des mots de passe.
#: 
#: Sans précision du paramètre --server, la clé ``'default'`` sera utilisée.
#: 
#: * ``'server_cmd'`` : La commande exécutée sur le client pour appeler
#:   le script sur le serveur distant.
servers = {
    'default': {
        'server_cmd': ['/home/dstan/cranspasswords/serverconfigs/tudor/cpasswords-server', ],
        'keep-alive': True,
    },
    'gladys': {
        'server_cmd': ['/usr/bin/ssh', 'home.b2moo.fr', '/home/dstan/cranspasswords/serverconfigs/tudor/cpasswords-server', ],
        'keep-alive': True,
    },
    'gladys-home': {
        'server_cmd': ['/usr/bin/ssh', 'gladys.home', '/home/dstan/cranspasswords/serverconfigs/tudor/cpasswords-server', ],
        'keep-alive': True,
    },
    'pimeys': {
        'server_cmd': ['/usr/bin/ssh', 'pimeys.fr', 'sudo', '-n', '/usr/local/bin/cranspasswords-server', ],
        'keep-alive': True,
    },
}
